from django.shortcuts import render

from django.http import JsonResponse

# Create your views here.
def operation(request, operation, first_number, second_number):
    if operation == "add": 
        operation = '+'
    elif operation == 'subtract':
        operation = '-'
    else:
        return JsonResponse({
            "error" : "Unexpected argument"
        }, status = 400)

    try:
        result =  eval(str(int(first_number)) + operation + str(int(second_number)))
    except:
        return JsonResponse({
            "error" : "Unexpected argument"
        }, status = 400)
    
    return JsonResponse({
        "result" : result
    })
